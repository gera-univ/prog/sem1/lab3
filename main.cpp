#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

bool property_greater(double a, double b) {
    return (a > b);
}

bool property_lower(double a, double b) {
    return (a < b);
}

bool property_zero(double a) {
    return a == 0;
}

bool property_notzero(double a) {
    return a != 0;
}

class Matrix {
public:
    Matrix(size_t rows, size_t columns) : mCols(columns), mRows(rows) {
        mData = new double *[rows];
        for (size_t i = 0; i < rows; ++i) {
            mData[i] = new double[columns];
        }
    }

    virtual ~Matrix() {
        for (size_t i = 0; i < mRows; ++i) {
            delete[] mData[i];
        }
        delete[] mData;
    }

    double &at(size_t row, size_t column) {
        return mData[row][column];
    }

    [[nodiscard]] const double &at(size_t row, size_t column) const {
        return mData[row][column];
    }

    [[nodiscard]] string toString() const {
        ostringstream result;
        result << std::setprecision(std::numeric_limits<double>::digits10);
        for (size_t i = 0; i < mRows; ++i) {
            for (size_t j = 0; j < mCols; ++j) {
                string str_value = to_string(mData[i][j]);
                size_t end = str_value.find_last_not_of('0') + 1;
                result << str_value.erase(end) << " ";
            }
            result << "\n";
        }
        return result.str();
    }

    Matrix withProperty(bool (*property)(double)) {
        /*
         * Возвращает новую матрицу, со строками, в которых
         * хотя бы для одного элемента property возвращает true
         */
        int *row_indices = new int[mRows];
        int row_count = 0;
        for (size_t i = 0; i < mRows; ++i) {
            bool row_has_property = hasProperty(true, i, property);
            if (row_has_property) {
                row_indices[row_count++] = i;
            }
        }
        Matrix result(row_count, mCols);
        for (size_t i = 0; i < row_count; ++i) {
            for (size_t j = 0; j < mCols; ++j) {
                result.at(i, j) = mData[row_indices[i]][j];
            }
        }
        delete[] row_indices;
        return result;
    }

    Matrix transpose() {
        Matrix result(mCols, mRows);
        for (size_t i = 0; i < mRows; ++i) {
            for (size_t j = 0; j < mCols; ++j) {
                result.at(j, i) = mData[i][j];
            }
        }
        return result;
    }

    double compareProperty(bool row, int index, bool (*property)(double, double)) {
        /*
         * Находит максимальный элемент в строке (row=true) или столбце (row=false)
         * по функции property
         */
        double result = (row) ? (mData[index][0]) : (mData[0][index]);
        for (size_t j = 1; j < ((row) ? (mCols) : (mRows)); ++j) {
            if (property(((row) ? (mData[index][j]) : (mData[j][index])), result)) {
                result = (row) ? (mData[index][j]) : (mData[j][index]);
            }
        }
        return result;
    }

    bool hasProperty(bool row, int index, bool (*property)(double)) {
        /*
         * Возвращает true, если хотя бы для одного элемента строки (row=true)
         * или столбце (row=false) с индексом index, property возвращает true
         */
        for (size_t j = 0; j < ((row) ? (mCols) : (mRows)); ++j) {
            if (property(((row) ? (mData[index][j]) : (mData[j][index]))))
                return true;
        }
        return false;
    }

    double sum(bool row, int index) {
        /*
         * Возвращает сумму строки (row=true) или столбца (row=false)
         * с индексом index
         */
        double result = 0;
        for (size_t j = 0; j < ((row) ? (mCols) : (mRows)); ++j) {
            result += (row) ? (mData[index][j]) : (mData[j][index]);
        }
        return result;
    }

    int firstZeroRow() {
        /*
         * Находит первую строку, равную нулю.
         * Иначе, возвращает -1
         */
        for (size_t i = 0; i < mRows; ++i) {
            if (hasProperty(true, i, property_zero))
                return i;
        }
        return -1;
    }

    int countSaddlePoints() {
        Matrix tData(mRows, mCols);
        for (size_t i = 0; i < mRows; ++i) {
            double minimal = compareProperty(true, i, property_lower);
            for (size_t j = 0; j < mCols; ++j) {
                if (mData[i][j] == minimal)
                    ++tData.at(i, j);
            }
        }

        int result = 0;
        for (size_t j = 0; j < mCols; ++j) {
            double maximal = compareProperty(false, j, property_greater);
            for (size_t i = 0; i < mRows; ++i)
                if (mData[i][j] == maximal && tData.at(i, j) == 0)
                    ++result;
        }
        return result;
    }

    friend istream &operator>>(istream &istr, Matrix &matrix);

private:
    double **mData;
    size_t mCols;
    size_t mRows;
};

ostream &operator<<(ostream &ostr, const Matrix &matrix) {
    return ostr << matrix.toString();
}

class matrixIncorrectInput : public exception {};

istream &operator>>(istream &istr, Matrix &matrix) {
    for (size_t i = 0; i < matrix.mRows; ++i) {
        for (size_t j = 0; j < matrix.mCols; ++j) {
            istr >> matrix.at(i, j);
            if (istr.fail()) {
                throw matrixIncorrectInput();
            }
        }
    }
    return istr;
}

void help(const string &program) {
    cout << "Usage: " << program << " [input_file] [ROWS] [COLUMNS]" << endl;
}

void calculate(Matrix &matrix, int H) {
    cout << "t. 1.5.1. Find sums of the rows with at least one zero:" << endl;
    bool has_rows_with_zero = false;
    for (int i = 0; i < H; ++i) {
        if (matrix.hasProperty(true, i, property_zero)) {
            has_rows_with_zero = true;
            cout << "Sum of the elements in row " << i << " is " << matrix.sum(true, i) << endl;
        }
    }
    if (!has_rows_with_zero)
        cout << "There are no zeroes in the matrix" << endl;

    cout << "t. 1.5.2 Find the number of saddle elements in the matrix:" << endl;
    int saddle_count = matrix.countSaddlePoints();
    if (saddle_count) {
        cout << saddle_count << endl;
    } else cout << "There are no saddle points in the matrix" << endl;

    auto new_matrix = matrix.withProperty(property_notzero).transpose().withProperty(property_notzero).transpose();
    cout << "t. 2.1.1 Build a new matrix, but without zero rows:" << endl;
    cout << new_matrix;

    cout << "t. 2.1.2 Find the row with the first zero:" << endl;
    int first_zero_row = new_matrix.firstZeroRow();
    if (first_zero_row != -1)
         cout << "Row " << first_zero_row << endl;
    else
        cout << "There are no zeroes in the matrix" << endl;
}

int main(int argc, char **argv) {
    string filename = "input.txt";

    int W = -1, H = -1;

    if (argc > 1) {
        if (static_cast<string>(argv[1]) == "--help") {
            help(argv[0]);
            return 0;
        } else {
            filename = argv[1];
            if (argc > 2) {
                try {
                    H = stoi(argv[2]);
                    W = stoi(argv[3]);
                } catch (logic_error) {
                    cout << "Incorrect parameters!" << endl;
                    help(argv[0]);
                    return -1;
                }
            }
        }
    }

    ifstream fin;
    fin.open(filename);

    if (fin.fail()) {
        cout << "Error occured while opening the file " << filename << endl;
        return -1;
    }

    if (cin.fail() || W < 1 || H < 1) {
        cout << "Error: Incorrect dimensions!" << endl;
        return -1;
    }
    Matrix matrix(H, W);
    try {
        fin >> matrix;
    }
    catch (matrixIncorrectInput) {
        cout << "Error: Incorrect input or matrix dimensions are too big!" << endl;
        return -1;
    }

    cout << "Lab 3\n" << matrix;

    calculate(matrix, H);

    fin.close();
}
